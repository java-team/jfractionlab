/**
*	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program. If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab;

import info.clearthought.layout.TableLayout;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import jfractionlab.displays.PointDisplay;
import jfractionlab.exerciseDialogs.ClickNumerator;
import jfractionlab.exerciseDialogs.CompareFractions;
import jfractionlab.exerciseDialogs.DecimalToFraction;
import jfractionlab.exerciseDialogs.DivideFractionsByFractions;
import jfractionlab.exerciseDialogs.DivideFractionsByNumbers;
import jfractionlab.exerciseDialogs.DivideNumbersByFractions;
import jfractionlab.exerciseDialogs.ExtendFraction;
import jfractionlab.exerciseDialogs.FractionToDecimal;
import jfractionlab.exerciseDialogs.ImproperFraction;
import jfractionlab.exerciseDialogs.MixedNumbers;
import jfractionlab.exerciseDialogs.MultiplyFractions;
import jfractionlab.exerciseDialogs.NameFraction;
import jfractionlab.exerciseDialogs.PlusAndMinus;
import jfractionlab.exerciseDialogs.ReduceFraction;
import jfractionlab.exerciseGenerator.ExerciseGenerator;
import jfractionlab.jflDialogs.ConfigureLO;
import jfractionlab.jflDialogs.InfoDialog;
import jfractionlab.jflDialogs.LogSaverDialog;
import jfractionlab.jflDialogs.WorkSheetDialog;
import jfractionlab.logtable.LogTable;


//TODO JFractionLab

// formulare als jar mitliefern und vom programm ins homeverzeichnis kopieren lassen

//neuer aufgabentyp: vergleichen durch gleichnamig machen?

//Hilfe: ****************************
//CompareFractions: hilfe schreiben und implementieren
//FractionToDecimal Hilfekapitel schreiben und hier verlinken!
//hilfe besser internationalisieren mit cmssimple?? "offline"-cms ohne datenbank??

//Worksheets: ***********************
//FractionToDecimal ADD WORKSHEET
//arbeitsblätter mit verschiedenen aufgabentypen ermoeglichen, dialog mit allen auswahlmöglichkeiten schreiben

//Release: ******************************
//document the code with javadoc

//FIXME jfl arbeitsblätter unechte brueche 90/24 = 3 18/24
//FIXME jfl arbeitsblätter multiplizieren wertebereich nur bis 20

public class JFractionLab extends JFrame implements ActionListener, ItemListener{
	public static final long serialVersionUID = 54L;
	public static final String jep_fontface="<font face='Avalon,Wide Latin'>";
	public static final Random ran = new Random(System.currentTimeMillis());
	public static final int nbOfUnoConfJars = 7;
	public static boolean worksheetProcessIsOK = true;
	private JMenuBar jmb = new JMenuBar();
	private JMenu jmLanguage = new JMenu("");
		private JMenuItem jmiGerman = new JMenuItem("");
		private JMenuItem jmiFrench = new JMenuItem("");
		private JMenuItem jmiSpanish = new JMenuItem("");
		private JMenuItem jmiEnglish = new JMenuItem("");
		private JMenuItem jmiItalian = new JMenuItem("");
		private JMenuItem jmiPortuguese = new JMenuItem("");
		private JMenuItem jmiGreek = new JMenuItem("");
		private JMenuItem jmiPolish = new JMenuItem("");
	private JMenu jmWorkSheet = new JMenu("");
		private JMenuItem jmiWorkSheetExtending = new JMenuItem("");
		private JMenuItem jmiWorkSheetReducing = new JMenuItem("");
		private JMenuItem jmiWorkSheetImproperFractions = new JMenuItem("");
		private JMenuItem jmiWorkSheetMixedNumbers = new JMenuItem("");
		private JMenuItem jmiWorksheetFractionToDecimal = new JMenuItem("");
		private JMenuItem jmiWorkSheetAddition = new JMenuItem("");
		private JMenuItem jmiWorkSheetSubtraction = new JMenuItem("");
		private JMenuItem jmiWorkSheetMultiplication = new JMenuItem("");
		private JMenuItem jmiWorkSheetDivision = new JMenuItem("");
		private JMenuItem jmiConfigurePathToLibreOffice;
	private JMenu jmHelp = new JMenu("");
	public static JCheckBoxMenuItem cb_showtippatstart = new JCheckBoxMenuItem();
	private JMenuItem jmiAbout = new JMenuItem("");
	private JMenuItem jmiHelp = new JMenuItem("");
	//GUI
	private TitledBorder brd_top;
	private JButton btn_clickNumerator;//click_the_numerator
	private PointDisplay dsp_clickNumerator = new PointDisplay(true);
	private JButton btn_nameFraction;//Nenne_den_Bruch
	private PointDisplay dsp_nameFraction = new PointDisplay(true);
	private JButton btn_compareFractions;//Vergleiche_Pizzen
	private PointDisplay dsp_compareFractions = new PointDisplay(true);
	private JButton btn_extendFraction;
	private PointDisplay dsp_extendFraction = new PointDisplay(true);
	private JButton btn_reduceFraction;
	private PointDisplay dsp_reduceFraction = new PointDisplay(true);
	private JButton btn_improperFracions;
	private PointDisplay dsp_improperFractions = new PointDisplay(true);
	private JButton btn_mixedNumbers;
	private PointDisplay dsp_mixedNumbers = new PointDisplay(true);
	private JButton btn_fractionToDecimal;
	private PointDisplay dsp_fractionToDecimal = new PointDisplay(true);
	
	private JButton btn_decimalToFraction;
	private PointDisplay dsp_decimalToFraction = new PointDisplay(true);
	
	private TitledBorder brd_bottom;
	private JButton btn_addFractions;
	private PointDisplay dsp_addFractions = new PointDisplay(true);
	private JButton btn_subtractFractions;
	private PointDisplay dsp_subtractFractions = new PointDisplay(true);
	private JButton btn_multiplyFractions;
	private PointDisplay dsp_multiplyFractions = new PointDisplay(true);
	private JButton btn_divideFractionsByNumbers;
	private PointDisplay dsp_divideFractionsByNumbers = new PointDisplay(true);
	private JButton btn_divideNumbersByFractions;
	private PointDisplay dsp_divideNumbersByFractions = new PointDisplay(true);
	private JButton btn_divideFractionsByFractions;
	private PointDisplay dsp_divideFractionsByFractions = new PointDisplay(true);
	private JButton btn_save;
	private JButton btn_readTheLog;

	private static final int location_x=50;
	private static final int location_y=100;
	private static final int size_x=1000;
	private static final int size_y=300;
	public static String os;
	
	public int points_clickNumerator = 0; //points in spiel 1 und korrekt bergeben?
	public int points_nameFraction = 0;
	public int points_compareFractions = 0;
	public int points_compareInvisibleFractions = 0;
	public int points_extendFraction = 0;
	public int points_reduceFraction = 0;
	public int points_unechteBrueche = 0;
	public int points_gemischteZahlen = 0;
	public int points_fractionToDecimal = 0;
	public int points_decimalToFraction = 0;
	public int points_addFractions = 0;
	public int points_subtractFractions = 0;
	public int points_multiplyFractions = 0;
	public int points_divideFractionsByNumbers = 0;
	public int points_divideNumbersByFractions = 0;
	public int points_divideFractionsByFractions = 0;
	
	//Damit alle Dialoge dieselbe Schrift haben, ist sie hier static definiert.
	public static Font infofont = new Font("SansSerif",Font.BOLD,24);
//	public static final int nbOfCols = 17;
	private String aboutText = "";
	
	/**
	* Create the GUI and show it.
	* For thread safety,
	* this method should be invoked from the
	* event-dispatching thread.
	* See http://java.sun.com/docs/books/tutorial/uiswing/learn/example1.html
	*/
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new JFractionLab();
			}//run
		});//invokeLater
//		JOptionPane.showMessageDialog(null,"Hallo Hendrik");
	}
	
	public JFractionLab() throws HeadlessException {
		makeGUI();
	}
	
	public void makeGUI(){
		os = System.getProperty("os.name").toLowerCase();
		setTitle("JFractionLab_version-number_lab");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		lang.Messages.setLocale(lang.Messages.getLocale());
		//Menue
		jmLanguage.setText(lang.Messages.getString("language"));
		
		jmiGerman.setText(lang.Messages.getString("Deutsch"));
		jmiGerman.addActionListener(this);
		jmLanguage.add(jmiGerman);

		jmiEnglish.setText(lang.Messages.getString("english"));
		jmiEnglish.addActionListener(this);
		jmLanguage.add(jmiEnglish);

		jmiSpanish.setText(lang.Messages.getString("spanish"));
		jmiSpanish.addActionListener(this);
		jmLanguage.add(jmiSpanish);
		
		jmiFrench.setText(lang.Messages.getString("french"));
		jmiFrench.addActionListener(this);
		jmLanguage.add(jmiFrench);
		
		jmiItalian.setText(lang.Messages.getString("italian"));
		jmiItalian.addActionListener(this);
		jmLanguage.add(jmiItalian);

		jmiPolish.setText(lang.Messages.getString("polish"));
		jmiPolish.addActionListener(this);
		jmLanguage.add(jmiPolish);
		
		jmiPortuguese.setText(lang.Messages.getString("portuguese"));
		jmiPortuguese.addActionListener(this);
		jmLanguage.add(jmiPortuguese);
		
		jmiGreek.setText(lang.Messages.getString("greek"));
		jmiGreek.addActionListener(this);
		jmLanguage.add(jmiGreek);
		
		jmWorkSheet.setText(lang.Messages.getString("worksheets"));
		
		jmiWorkSheetExtending.addActionListener(this);
		jmiWorkSheetExtending.setText(lang.Messages.getString("extend_fractions"));
		jmWorkSheet.add(jmiWorkSheetExtending);
		
		jmiWorkSheetReducing.addActionListener(this);
		jmiWorkSheetReducing.setText(lang.Messages.getString("reduce_fractions"));
		jmWorkSheet.add(jmiWorkSheetReducing);
		
		jmiWorkSheetImproperFractions.addActionListener(this);
		jmiWorkSheetImproperFractions.setText(lang.Messages.getString("improper_fracs"));
		jmWorkSheet.add(jmiWorkSheetImproperFractions);
		
		jmiWorkSheetMixedNumbers.addActionListener(this);
		jmiWorkSheetMixedNumbers.setText(lang.Messages.getString("mixed_numbers"));
		jmWorkSheet.add(jmiWorkSheetMixedNumbers);

		jmiWorksheetFractionToDecimal.addActionListener(this);
		jmiWorksheetFractionToDecimal.setText(lang.Messages.getString("fraction_to_decimal"));
		jmWorkSheet.add(jmiWorksheetFractionToDecimal);
		
		jmiWorkSheetAddition.addActionListener(this);
		jmiWorkSheetAddition.setText(lang.Messages.getString("add_fractions"));
		jmWorkSheet.add(jmiWorkSheetAddition);

		jmiWorkSheetSubtraction.addActionListener(this);
		jmiWorkSheetSubtraction.setText(lang.Messages.getString("minus_fractions"));
		jmWorkSheet.add(jmiWorkSheetSubtraction);

		jmiWorkSheetMultiplication.addActionListener(this);
		jmiWorkSheetMultiplication.setText(lang.Messages.getString("multiply_fractions"));
		jmWorkSheet.add(jmiWorkSheetMultiplication);
		
		jmiWorkSheetDivision.addActionListener(this);
		jmiWorkSheetDivision.setText(lang.Messages.getString("div_fr_by_fr"));
		jmWorkSheet.add(jmiWorkSheetDivision);

		jmWorkSheet.addSeparator();
		jmiConfigurePathToLibreOffice = new JMenuItem();
		jmiConfigurePathToLibreOffice.addActionListener(this);
		jmiConfigurePathToLibreOffice.setText(lang.Messages.getString("configure_libreoffice"));
		jmWorkSheet.add(jmiConfigurePathToLibreOffice);
		
		jmHelp.setText(lang.Messages.getString("jmi_help"));
		
		cb_showtippatstart.setText(lang.Messages.getString("show_hints_on_start"));
		cb_showtippatstart.addItemListener(this);
		setCheckBoxValue();
		jmHelp.add(cb_showtippatstart);
		
		jmHelp.addSeparator();
	
		jmiHelp.setText(lang.Messages.getString("jmi_help"));
		jmiHelp.addActionListener(this);
		jmHelp.add(jmiHelp);
		
		jmiAbout.setText(lang.Messages.getString("about_jfl"));
		jmiAbout.addActionListener(this);
		jmHelp.add(jmiAbout);
				
		jmb.add(jmLanguage);
		jmb.add(jmWorkSheet);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
		double sizes_main[][] = {{
			//Spalten
			TableLayout.FILL,
		},{
			//Zeilen
			0.88/14*8,//pn_oben
			0.88/14*6,//pn_unten
			TableLayout.FILL,//pn_steuer
		}};
		
		double sizes_oben[][] = {{
			// Spalten
			0.5,0.3,TableLayout.FILL
		},{
			//Zeilen
			TableLayout.FILL,//0
			TableLayout.FILL,//1
			TableLayout.FILL,//2
			TableLayout.FILL,//3
			TableLayout.FILL,//4
			TableLayout.FILL,//5
			TableLayout.FILL,//6
			TableLayout.FILL,//7
			TableLayout.FILL//8
		}};
		double sizes_unten[][] = {{
			// Spalten
			0.5,0.3,TableLayout.FILL
		},{
			//Zeilen
			TableLayout.FILL,//0
			TableLayout.FILL,//1
			TableLayout.FILL,//2
			TableLayout.FILL,//3
			TableLayout.FILL,//4
			TableLayout.FILL,//5
		}};
		Container cp = getContentPane();
        	cp.setLayout(new TableLayout(sizes_main));
        	JPanel pn_oben = new JPanel(new TableLayout(sizes_oben));
		JPanel pn_unten = new JPanel(new TableLayout(sizes_unten));
		JPanel pn_steuer = new JPanel(new GridLayout(2,1));
			btn_clickNumerator = new JButton("1. "+lang.Messages.getString("click_numerator"));
			btn_clickNumerator.addActionListener(this);
		pn_oben.add(btn_clickNumerator, "0,0,1,0");
		pn_oben.add(dsp_clickNumerator, "2,0");
		
			btn_nameFraction = new JButton("2. "+lang.Messages.getString("name_fractions"));
			btn_nameFraction.addActionListener(this);
		pn_oben.add(btn_nameFraction, "0,1,1,1");
		pn_oben.add(dsp_nameFraction, "2,1");
			btn_compareFractions = new JButton("3. "+lang.Messages.getString("compare_fractions"));
			btn_compareFractions.addActionListener(this);
		pn_oben.add(btn_compareFractions, "0,2,1,2");
		pn_oben.add(dsp_compareFractions, "2,2");
			btn_extendFraction = new JButton("4. "+lang.Messages.getString("extend_fractions"));
			btn_extendFraction.addActionListener(this);
		pn_oben.add(btn_extendFraction, "0,3,1,3");
		pn_oben.add(dsp_extendFraction, "2,3");
			btn_reduceFraction = new JButton("5. "+lang.Messages.getString("reduce_fractions"));
			btn_reduceFraction.addActionListener(this);
		pn_oben.add(btn_reduceFraction, "0,4,1,4");
		pn_oben.add(dsp_reduceFraction, "2,4");
			btn_improperFracions = new JButton("6. "+lang.Messages.getString("improper_fracs"));
			btn_improperFracions.addActionListener(this);
		pn_oben.add(btn_improperFracions, "0,5,1,5");
		pn_oben.add(dsp_improperFractions, "2,5");
			btn_mixedNumbers = new JButton("7. "+lang.Messages.getString("mixed_numbers"));
			btn_mixedNumbers.addActionListener(this);
		pn_oben.add(btn_mixedNumbers, "0,6,1,6");
		pn_oben.add(dsp_mixedNumbers, "2,6");
			btn_fractionToDecimal = new JButton("8. "+lang.Messages.getString("fraction_to_decimal"));
			btn_fractionToDecimal.addActionListener(this);
		pn_oben.add(btn_fractionToDecimal, "0,7,1,7");
		pn_oben.add(dsp_fractionToDecimal, "2,7");
			btn_decimalToFraction = new JButton("9. "+ lang.Messages.getString("decimal_to_fraction"));
			btn_decimalToFraction.addActionListener(this);
		pn_oben.add(btn_decimalToFraction, "0,8,1,8");
		pn_oben.add(dsp_decimalToFraction, "2,8");
		brd_top = new TitledBorder(new EtchedBorder(), lang.Messages.getString("learn_fractions"));
		pn_oben.setBorder(brd_top);
		cp.add(pn_oben, "0,0");
			btn_addFractions = new JButton("10. "+lang.Messages.getString("add_fractions"));
			btn_addFractions.addActionListener(this);
		pn_unten.add(btn_addFractions, "0,0,1,0");
		pn_unten.add(dsp_addFractions, "2,0");
			btn_subtractFractions = new JButton("11. "+lang.Messages.getString("minus_fractions"));
			btn_subtractFractions.addActionListener(this);
		pn_unten.add(btn_subtractFractions, "0,1,1,1");
		pn_unten.add(dsp_subtractFractions, "2,1");
			btn_multiplyFractions = new JButton("12. "+lang.Messages.getString("multiply_fractions"));
			btn_multiplyFractions.addActionListener(this);
		pn_unten.add(btn_multiplyFractions, "0,2,1,2");
		pn_unten.add(dsp_multiplyFractions, "2,2");
			btn_divideFractionsByNumbers = new JButton("13. "+lang.Messages.getString("div_fr_by_nb"));
			btn_divideFractionsByNumbers.addActionListener(this);
		pn_unten.add(btn_divideFractionsByNumbers, "0,3,1,3");
		pn_unten.add(dsp_divideFractionsByNumbers, "2,3");
			btn_divideNumbersByFractions = new JButton("14. "+lang.Messages.getString("div_nb_by_fr"));
			btn_divideNumbersByFractions.addActionListener(this);
		pn_unten.add(btn_divideNumbersByFractions, "0,4,1,4");
		pn_unten.add(dsp_divideNumbersByFractions, "2,4");
			btn_divideFractionsByFractions = new JButton("15. "+lang.Messages.getString("div_fr_by_fr"));
			btn_divideFractionsByFractions.addActionListener(this);
		pn_unten.add(btn_divideFractionsByFractions, "0,5,1,5");
		pn_unten.add(dsp_divideFractionsByFractions, "2,5");
		//pn_unten.setBorder(new CompoundBorder(new EmptyBorder(1,1,1,1),new EtchedBorder() ));
		brd_bottom = new TitledBorder(new EtchedBorder(), lang.Messages.getString("calculate_fractions"));
		pn_unten.setBorder(brd_bottom);
		cp.add(pn_unten, "0,1");
		
			btn_save = new JButton(lang.Messages.getString("save_results"));
			btn_save.addActionListener(this);
		pn_steuer.add(btn_save);
			btn_readTheLog = new JButton(lang.Messages.getString("read_logs"));
			btn_readTheLog.addActionListener(this);
		pn_steuer.add(btn_readTheLog);
		pn_steuer.setBorder(new CompoundBorder(new EmptyBorder(1,1,1,1),new EtchedBorder() ));
		cp.add(pn_steuer, "0,2");
		
		setLocation(400,50);
		setSize(450, 650);
		setMinimumSize(new Dimension(410, 460));
		setResizable(true);
		//JFrame.setDefaultLookAndFeelDecorated(true);
		try{
			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
			//see http://www.pushing-pixels.org/2008/07/13/swing-applications-and-mac-os-x-menu-bar.html
			if(System.getProperty("os.name").toLowerCase().contains("mac")){
				UIManager.put("MenuBarUI",UIManager.getSystemLookAndFeelClassName());
				UIManager.put("MenuUI",UIManager.getSystemLookAndFeelClassName());
				UIManager.put("MenuItemUI",UIManager.getSystemLookAndFeelClassName());
				UIManager.put("CheckBoxMenuItemUI",UIManager.getSystemLookAndFeelClassName());
				UIManager.put("RadioButtonMenuItemUI",UIManager.getSystemLookAndFeelClassName());
				UIManager.put("PopupMenuUI",UIManager.getSystemLookAndFeelClassName());
			}
		}catch(Exception exc){
			exc.printStackTrace();
			System.out.println("UIManager Problem");
		}
		setVisible(true);

		aboutText =
			lang.Messages.getString("gnu_gpl")+"<br>"
			+ lang.Messages.getString("copyright")+"<br>"
			+ lang.Messages.getString("url_text")+"<br>"
			+ lang.Messages.getString("emailaddress")+"<br>"
			+"<br>"
			+ lang.Messages.getString("thanks_to")+"<br><ul><li>"
			//x.oswald@free.fr
			//y.durand@gmx.de
			+ lang.Messages.getString("french_translators")+"</li><li>"
			//pablo.pita@pitagoral.com
			+ lang.Messages.getString("spanish_translators")+"</li><li>"
			//gudrungeorges@gmail.com
			+ lang.Messages.getString("english_translators")+"</li><li>"
			//Loupiotebec@aol.com
			//mirco.bergamini@yahoo.it
			+ lang.Messages.getString("italian_translators")+"</li><li>"
			//khemis@bol.com.br
			+ lang.Messages.getString("portuguese_translators")+"</li><li>"
			//passalis@csd.auth.gr
			+ lang.Messages.getString("greek_translators")+"</li><li>"
			+ lang.Messages.getString("polish_translators")+"</li><li>"
			//rafalsurynt@gmail.com
			//jredrejo@edu.juntaextremadura.net>
			+ lang.Messages.getString("debian_package_maintainer")+"</li><li>"
			+ lang.Messages.getString("icon_designer")+"</li><li>"
			+"Darryl Burke, http://tips4java.wordpress.com</li><li>"
			+"Daniel Barbalace, https://tablelayout.dev.java.net/</li></ul>"
		;//String
		
		
		//some properties
//		System.out.println("liste der properties = ");
//		Properties sysprops   = System.getProperties();
//		Enumeration<?> propnames = sysprops.propertyNames();
//		while (propnames.hasMoreElements()) {
//			String propname = (String)propnames.nextElement();
//			System.out.println(
//					propname + "=" + System.getProperty(propname)
//			);
//		}
		System.out.println("CLASSPATH = "+System.getProperty("java.class.path"));
	}//makeGUI
	
	/**
	 * 
	 * @param p
	 * @param prog
	 */
	public void setPoints(int p, String prog){
		if (prog == "clickNumerator"){
			points_clickNumerator = p;
			dsp_clickNumerator.setText(String.valueOf(p));
		}else if (prog == "nameFraction"){
			points_nameFraction = p;
			dsp_nameFraction.setText(String.valueOf(p));
		}else if (prog == "compareFractions"){
			points_compareFractions = p;
			dsp_compareFractions.setText(String.valueOf(p));
		}else if (prog == "extendFraction"){
			points_extendFraction = p;
			dsp_extendFraction.setText(String.valueOf(p));
		}else if (prog == "reduceFraction"){
			points_reduceFraction = p;
			dsp_reduceFraction.setText(String.valueOf(p));
		}else if (prog == "unechteBrueche"){
			points_unechteBrueche = p;
			dsp_improperFractions.setText(String.valueOf(p));
		}else if (prog == "gemischteZahlen"){
			points_gemischteZahlen = p;
			dsp_mixedNumbers.setText(String.valueOf(p));		
		}else if (prog == "fractiontodecimal"){
			points_fractionToDecimal = p;
			dsp_fractionToDecimal.setText(String.valueOf(p));
		}else if (prog == "decimalToFraction"){
			points_decimalToFraction = p;
			dsp_decimalToFraction.setText(String.valueOf(p));
		}else if (prog == "addFractions"){
			points_addFractions = p;
			dsp_addFractions.setText(String.valueOf(p));
		}else if (prog == "subtractFractions"){
			points_subtractFractions = p;
			dsp_subtractFractions.setText(String.valueOf(p));
		}else if (prog == "multiplyFractions"){
			points_multiplyFractions = p;
			dsp_multiplyFractions.setText(String.valueOf(p));
		}else if (prog == "divideFractionsByNumbers"){
			points_divideFractionsByNumbers = p;
			dsp_divideFractionsByNumbers.setText(String.valueOf(p));
		}else if (prog == "divideNumbersByFractions"){
			points_divideNumbersByFractions = p;
			dsp_divideNumbersByFractions.setText(String.valueOf(p));
		}else if (prog == "divideFractionsByFractions"){
			points_divideFractionsByFractions = p;
			dsp_divideFractionsByFractions.setText(String.valueOf(p));
		}else{
			throw new IllegalArgumentException();
		}
	}//public void setPoints(int p, String prog){
	
	
	/**
	 * 
	 *
	 */
	private void refreshCaption(){
		jmLanguage.setText(lang.Messages.getString("language"));
		jmiGerman.setText(lang.Messages.getString("Deutsch"));
		jmiFrench.setText(lang.Messages.getString("french"));
		jmiSpanish.setText(lang.Messages.getString("spanish"));
		jmiEnglish.setText(lang.Messages.getString("english"));
		jmiItalian.setText(lang.Messages.getString("italian"));
		jmiPortuguese.setText(lang.Messages.getString("portuguese"));
		jmiGreek.setText(lang.Messages.getString("greek"));
		jmiPolish.setText(lang.Messages.getString("polish"));
		jmWorkSheet.setText(lang.Messages.getString("worksheets"));
		jmiWorkSheetExtending.setText(lang.Messages.getString("extend_fractions"));
		jmiWorkSheetReducing.setText(lang.Messages.getString("reduce_fractions"));
		jmiWorkSheetImproperFractions.setText(lang.Messages.getString("improper_fracs"));
		jmiWorkSheetMixedNumbers.setText(lang.Messages.getString("mixed_numbers"));
		jmiWorksheetFractionToDecimal.setText(lang.Messages.getString("fraction_to_decimal"));
		jmiWorkSheetAddition.setText(lang.Messages.getString("add_fractions"));
		jmiWorkSheetSubtraction.setText(lang.Messages.getString("minus_fractions"));
		jmiWorkSheetMultiplication.setText(lang.Messages.getString("multiply_fractions"));
		jmiWorkSheetDivision.setText(lang.Messages.getString("div_fr_by_fr"));
		jmiConfigurePathToLibreOffice.setText(lang.Messages.getString("configure_libreoffice"));

		jmHelp.setText(lang.Messages.getString("Info"));
		jmiHelp.setText(lang.Messages.getString("jmi_help"));
		cb_showtippatstart.setText(lang.Messages.getString("show_hints_on_start"));
		jmiAbout.setText(lang.Messages.getString("about_jfl"));
		
		brd_top.setTitle(lang.Messages.getString("learn_fractions"));
		btn_clickNumerator.setText("1. "+lang.Messages.getString("click_numerator"));
		btn_nameFraction.setText("2. "+lang.Messages.getString("name_fractions"));
		btn_compareFractions.setText("3. "+lang.Messages.getString("compare_fractions"));
		btn_extendFraction.setText("4. "+lang.Messages.getString("extend_fractions"));
		btn_reduceFraction.setText("5. "+lang.Messages.getString("reduce_fractions"));
		btn_improperFracions.setText("6. "+lang.Messages.getString("improper_fracs"));
		btn_mixedNumbers.setText("7. "+lang.Messages.getString("mixed_numbers"));
		btn_fractionToDecimal.setText("8. "+lang.Messages.getString("fraction_to_decimal"));
		btn_decimalToFraction.setText("9. " + lang.Messages.getString("decimal_to_fraction"));
		
		brd_bottom.setTitle(lang.Messages.getString("calculate_fractions"));
		btn_addFractions.setText("9. "+lang.Messages.getString("add_fractions"));
		btn_subtractFractions.setText("10. "+lang.Messages.getString("minus_fractions"));
		btn_multiplyFractions.setText("11. "+lang.Messages.getString("multiply_fractions"));
		btn_divideFractionsByNumbers.setText("12. "+lang.Messages.getString("div_fr_by_nb"));
		btn_divideNumbersByFractions.setText("13. "+lang.Messages.getString("div_nb_by_fr"));
		btn_divideFractionsByFractions.setText("14. "+lang.Messages.getString("div_fr_by_fr"));
		
		btn_save.setText(lang.Messages.getString("save_results"));
		btn_readTheLog.setText(lang.Messages.getString("read_logs"));
	}//refreshCaption
	
	public static void setCheckBoxValue(){
		ConfManager cm = new ConfManager();
		if(cm.isTipAtStart()){
			cb_showtippatstart.setSelected(true);
		}else{
			cb_showtippatstart.setSelected(false);
		}
		
	}
	
	public void itemStateChanged(ItemEvent e){
		//System.out.println("==itemStateChanged");
		Object obj = e.getSource();
		if (obj == cb_showtippatstart){
			if(cb_showtippatstart.isSelected()){
				new ConfManager().setTipAtStart(true);
			}else{
				new ConfManager().setTipAtStart(false);
			}
		}
	}//itemStateChanged
	
	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
			Object obj = e.getSource();
        	if (obj == btn_clickNumerator){
        		new ClickNumerator(this,location_x, location_y, size_x-230, size_y+150);
        	}else if (obj == btn_nameFraction){
        		new NameFraction(this,location_x, location_y, size_x-130, size_y+150);
        	}else if (obj == btn_compareFractions){
        		new CompareFractions(this,location_x, location_y, size_x-50, size_y+200);
        	}else if (obj == btn_extendFraction){
        		new ExtendFraction(this,location_x, location_y, size_x, size_y+200);
        	}else if (obj == btn_reduceFraction){
        		new ReduceFraction(this,location_x, location_y, size_x, size_y+200);
        	}else if (obj == btn_improperFracions){
        		new ImproperFraction(this,location_x, location_y, size_x, size_y+200);
        	}else if (obj == btn_mixedNumbers){
        		new MixedNumbers(this,location_x, location_y, size_x, size_y+200);
        	}else if (obj == btn_fractionToDecimal){
        		new FractionToDecimal(this,location_x, location_y, size_x, size_y+200);
        	}else if (obj == btn_decimalToFraction){
        		new DecimalToFraction(this,location_x, location_y, size_x, size_y+200);
        	}else if (obj == btn_addFractions){
        		new PlusAndMinus(this, "+", location_x, location_y, size_x + 100, size_y + 60);
        	}else if (obj == btn_subtractFractions){
        		new PlusAndMinus(this, "-", location_x, location_y, size_x + 100, size_y);
        	}else if (obj == btn_multiplyFractions){
        		new MultiplyFractions(this, location_x, location_y, size_x, size_y);
        	}else if (obj == btn_divideFractionsByNumbers){
        		new DivideFractionsByNumbers(this, location_x, location_y, size_x, size_y);
        	}else if (obj == btn_divideNumbersByFractions){
        		new DivideNumbersByFractions(this, location_x, location_y, size_x, size_y+150);
        	}else if (obj == btn_divideFractionsByFractions){
        		new DivideFractionsByFractions(this, location_x, location_y, size_x, size_y);
        	}else if (obj == btn_save){
        		LogSaverDialog lsd = new LogSaverDialog(
        			points_clickNumerator,
        			points_nameFraction,
        			points_compareFractions,
        			points_extendFraction,
        			points_reduceFraction,
        			points_unechteBrueche,
        			points_gemischteZahlen,
        			points_fractionToDecimal,
        			points_decimalToFraction,
        			points_addFractions,
        			points_subtractFractions,
        			points_multiplyFractions,
        			points_divideFractionsByNumbers,
        			points_divideNumbersByFractions,
        			points_divideFractionsByFractions
        		);//LogSaverDialog
        		if(lsd.isSavedSuccessfully){
	        		setPoints(0, "clickNumerator");
	    			setPoints(0, "nameFraction");
	    			setPoints(0, "compareFractions");
	    			setPoints(0, "extendFraction");
	    			setPoints(0, "reduceFraction");
	    			setPoints(0, "unechteBrueche");
	    			setPoints(0, "gemischteZahlen");
	    			setPoints(0, "fractiontodecimal");
	    			setPoints(0, "decimalToFraction");
	    			setPoints(0, "addFractions");
	    			setPoints(0, "subtractFractions");
	    			setPoints(0, "multiplyFractions");
	    			setPoints(0, "divideFractionsByNumbers");
	    			setPoints(0, "divideNumbersByFractions");
	    			setPoints(0, "divideFractionsByFractions");
        		}
		}else if (obj == btn_readTheLog){
			JOptionPane.showMessageDialog(
				null,
				lang.Messages.getString("choose_logdir")
			);
			JFileChooser fc = new JFileChooser();
			ResultOfGame sl = new ResultOfGame();
			//warum hat setLocale keine Wirkung?
			fc.setLocale(lang.Messages.getLocale());
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = fc.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				String[] dir = file.list(new MyFilenameFilter("jfl"));
				String[] colnames = {
						"Name","Date",
						lang.Messages.getString("click_numerator"),
						lang.Messages.getString("name_fractions"),
						lang.Messages.getString("compare_fractions"),
						lang.Messages.getString("extend_fractions"),
						lang.Messages.getString("reduce_fractions"),
						lang.Messages.getString("improper_fracs"),
						lang.Messages.getString("mixed_numbers"),
						lang.Messages.getString("fraction_to_decimal"),
						lang.Messages.getString("decimal_to_fraction"),
						lang.Messages.getString("add_fractions"),
						lang.Messages.getString("minus_fractions"),
						lang.Messages.getString("multiply_fractions"),
						lang.Messages.getString("div_fr_by_nb"),
						lang.Messages.getString("div_nb_by_fr"),
						lang.Messages.getString("div_fr_by_fr")
				};
				String[][] data = new String[dir.length][colnames.length];
				java.util.Arrays.sort(dir);
				for(int i=0; i<dir.length; i++){
					try{
						FileInputStream fs = new FileInputStream(file+"/"+dir[i]);
						ObjectInputStream is = new ObjectInputStream(fs);
						sl = (ResultOfGame)is.readObject();
						is.close();
						data[i][0] = sl.getPlayerName();
						data[i][1] = sl.getTimeStamp();
						if(sl.getPtsCN() == 0){data[i][2] = " ";
						}else{data[i][2] = String.valueOf(sl.getPtsCN());}
						
						if(sl.getPtsNF()==0){
							data[i][3] = " ";
						}else{
							data[i][3] = String.valueOf(sl.getPtsNF());
						}
						
						if(sl.getPtsCF()==0){
							data[i][4] = " ";
						}else{
							data[i][4] = String.valueOf(sl.getPtsCF());
						}
						
						if(sl.getPtsEF()==0){
							data[i][5] = " ";
						}else{
							data[i][5] = String.valueOf(sl.getPtsEF());
						}
						
						if(sl.getPtsRF()==0){
							data[i][6] = " ";
						}else{
							data[i][6] = String.valueOf(sl.getPtsRF());
						}
						
						if(sl.getPtsIF()==0){
							data[i][7] = " ";
						}else{
							data[i][7] = String.valueOf(sl.getPtsIF());
						}
						
						if(sl.getPtsMN()==0){
							data[i][8] = " ";
						}else{
							data[i][8] = String.valueOf(sl.getPtsMN());
						}
						
						if(sl.getPtsFtD()==0){
							data[i][9] = " ";
						}else{
							data[i][9] = String.valueOf(sl.getPtsFtD());
						}
						
						if(sl.getPtsDtF()==0){
							data[i][10] = " ";
						}else{
							data[i][10] = String.valueOf(sl.getPtsDtF());
						}
						
						if(sl.getPtsPL()==0){
							data[i][11] = " ";
						}else{
							data[i][11] = String.valueOf(sl.getPtsPL());
						}
						
						if(sl.getPtsMIN()==0){
							data[i][12] = " ";
						}else{
							data[i][12] = String.valueOf(sl.getPtsMIN());
						}
						
						if(sl.getPtsMF()==0){
							data[i][13] = " ";
						}else{
							data[i][13] = String.valueOf(sl.getPtsMF());
						}
						
						if(sl.getPtsDFN()==0){
							data[i][14] = " ";
						}else{
							data[i][14] = String.valueOf(sl.getPtsDFN());
						}
						
						if(sl.getPtsDNF()==0){
							data[i][15] = " ";
						}else{
							data[i][15] = String.valueOf(sl.getPtsDNF());
						}
						
						if(sl.getPtsDFF()==0){
							data[i][16] = " ";
						}else{
							data[i][16] = String.valueOf(sl.getPtsDFF());
						}
						
					}catch( Exception ex ) {
						ex.printStackTrace();
						JOptionPane.showMessageDialog(
							null,
							lang.Messages.getString("file_can_not_be_opened")
						);
					}//acatch
				}//for
				new LogTable(this, data, colnames);
			}//returnVal
		}else if (obj == jmiGerman){
			lang.Messages.setLocale(Locale.GERMAN);
			refreshCaption();
		}else if (obj == jmiFrench){
			lang.Messages.setLocale(Locale.FRANCE);
			refreshCaption();
		}else if (obj == jmiSpanish){
			lang.Messages.setLocale(new Locale("es"));
			refreshCaption();
		}else if (obj == jmiEnglish){
			lang.Messages.setLocale(Locale.ENGLISH);
			refreshCaption();
		}else if (obj == jmiItalian){
			lang.Messages.setLocale(Locale.ITALIAN);
			refreshCaption();
		}else if (obj == jmiPortuguese){
			lang.Messages.setLocale(new Locale("pt"));
			refreshCaption();
		}else if(obj == jmiGreek){
			lang.Messages.setLocale(new Locale("gr"));
			refreshCaption();
		}
		else if (obj == jmiPolish){
			lang.Messages.setLocale(new Locale("pl"));
			refreshCaption();
		}
		else if(obj == jmiWorkSheetExtending){
			new WorkSheetDialog(
					ExerciseGenerator.EXTEND_FRACTIONS,
					lang.Messages.getString("extend_fractions"),
					lang.Messages.getString("fractions"),
					lang.Messages.getString("factors")
			);
		}else if(obj == jmiWorkSheetReducing){
			new WorkSheetDialog(
					ExerciseGenerator.REDUCE_FRACTIONS,
					lang.Messages.getString("reduce_fractions"),
					lang.Messages.getString("fractions"),
					lang.Messages.getString("factors")
			);
		}else if(obj == jmiWorkSheetImproperFractions){
			new WorkSheetDialog(
					ExerciseGenerator.IMPROPER_FRACTIONS,
					lang.Messages.getString("improper_fracs"),
					lang.Messages.getString("fractions")
			);
		}else if(obj == jmiWorkSheetMixedNumbers){
			new WorkSheetDialog(
					ExerciseGenerator.MIXED_NUMBERS,
					lang.Messages.getString("mixed_numbers"),
					lang.Messages.getString("full_numbers"),
					lang.Messages.getString("fractions")
			);
		}else if(obj == jmiWorksheetFractionToDecimal){
			new WorkSheetDialog(
					ExerciseGenerator.FRACTIONS_TO_DECIMAL,
					lang.Messages.getString("fraction_to_decimal")
			);
		}else if(obj == jmiWorkSheetAddition){
			new WorkSheetDialog(
					ExerciseGenerator.ADD_FRACTIONS,
					lang.Messages.getString("add_fractions")
			);
		}else if(obj == jmiWorkSheetSubtraction){
			new WorkSheetDialog(
					ExerciseGenerator.SUBTRACT_FRACTIONS,
					lang.Messages.getString("minus_fractions")
			);
		}else if(obj == jmiWorkSheetMultiplication){
			new WorkSheetDialog(
					ExerciseGenerator.MULTIPLY_FRACTIONS,
					lang.Messages.getString("multiply_fractions"),
					lang.Messages.getString("fractions")
			);
		}else if(obj == jmiWorkSheetDivision){
			new WorkSheetDialog(
					ExerciseGenerator.DIVIDE_FRACTIONS,
					lang.Messages.getString("div_fr_by_fr"),
					lang.Messages.getString("fractions")
			);
		}else if (obj == jmiConfigurePathToLibreOffice){
			new ConfigureLO();
		}
		else if (obj == jmiHelp){
			new HelpStarter(
					lang.Messages.getLocale().toString(),
					"index"
			);
		}else if (obj == jmiAbout){
			new InfoDialog(lang.Messages.getString("about_jfl"), aboutText);
		}//if else
	}//actionPerformed
	
	public static int greatestCommonDivisor(int a, int b){
		// verwendet wird der erweiterte Euklidsche Algorithmus
		if(a==b||b==0) return a;
		else return greatestCommonDivisor(b,a%b);
	}

	public static int leastComonMultiple(int a, int b){
		// verwendet wird der Satz ggT(a,b) * kgV(a,b) = a*b
		return (a*b)/greatestCommonDivisor(a,b);
	}
}//class
