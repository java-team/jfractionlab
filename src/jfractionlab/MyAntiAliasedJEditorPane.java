package jfractionlab;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;

import javax.swing.JEditorPane;

public class MyAntiAliasedJEditorPane extends JEditorPane {
	static final long serialVersionUID =jfractionlab.JFractionLab.serialVersionUID;
	
	private Key key1 = RenderingHints.KEY_TEXT_ANTIALIASING;
	private Object value1 = RenderingHints.VALUE_TEXT_ANTIALIAS_ON;

	private Key key2 = RenderingHints.KEY_ANTIALIASING;
	private Object value2 = RenderingHints.VALUE_ANTIALIAS_ON;
	
	public MyAntiAliasedJEditorPane(){
		super();
	}
	
	protected void paintComponent(Graphics g){
		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(key1, value1);
		g2.setRenderingHint(key2, value2);
		super.paintComponent(g2);
	}
}
