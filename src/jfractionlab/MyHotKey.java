package jfractionlab;

import javax.swing.JButton;
import javax.swing.JMenu;

public class MyHotKey {

	public static void setTextWithHotKey(JMenu jm, String str){
		if(str.contains("&")){
			jm.setText(getCaption(str));
			jm.setMnemonic(getHotKey(str));
		}
	}

	public static void setTextWithHotKey(JButton jm, String str){
		if(str.contains("&")){
			jm.setText(getCaption(str));
			jm.setMnemonic(getHotKey(str));
		}
	}	
	
	private static String getCaption(String str){
		String out="";
		if(str.contains("&")){
			String[] arstr = str.split("&");
			for(int i = 0; i< arstr.length; i++){
				out += arstr[i];
			}
		}else{
			out = str;
		}
		return out;
	}

	private static char getHotKey(String str){
		char cr = ' ';
		if(str.contains("&")){
			int i = str.indexOf('&');
			cr = str.substring(i+1, i+2).charAt(0);
		}
		return cr;
	}
}
