/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.displays;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Arc2D;

import jfractionlab.JFractionLab;
import jfractionlab.exerciseDialogs.ClickNumerator;

public class FractionAsClickCircle extends Canvas implements MouseListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
		private ClickNumerator owner;
		private int numerator = 1;
		private int denominator = 8;
		private int x_center;
		private int y_center;
		private boolean[] filledsectors;

		/**
		 * 
		 */
		public FractionAsClickCircle(ClickNumerator owner){
			setBackground(Color.white);
			addMouseListener(this);
			this.owner = owner;
		}//public FractionAsClickCircle

		/**
		 * 
		 *
		 */
		private void newInit(){
			filledsectors = new boolean[denominator];
			repaint();
		}//newIni

		/**
		 * 
		 * @param z
		 * @param n
		 */
		public void setFraction(int z, int n){
			this.numerator = z;
			this.denominator = n;
			newInit();
		}
		
		/**
		 * 
		 * @return
		 */
		public String check(){
			int clicked=0;
			for(int j = 0; j<denominator;j++){
				if(filledsectors[j]){
					clicked++;
				}
			}
			if (clicked == numerator){
				//award++;
				return "0";
			}else if (clicked < numerator){
				return "-";
			}else if (clicked > numerator){
				return "+";
			}else{
				return "something is wrong";
			}
		}

		/**
		 * 
		 * @param x
		 * @param y
		 * @return
		 */
		private double calculateWinkel(int x, int y){
			double PI = 3.14159265;
			double winkel = 0.0;
			/*
			* tangenz_alpha = gegenkathete/ankathete
			* atan(tangenz_alpha) liefert den winkel im bogenmaß
			*	atan(tangenz_alpha) calculates the angle in (2PI ?circular measure)
			* je nach quadrant muss der winkel ins gradmaumgerechnet werden
			*	depending on the quarter the angle has to calculated in (360° ?degrees)
			*/
			if (x >= x_center && y <= y_center){
			//1. quadrant up right
				winkel = java.lang.Math.atan((double)(y_center-y)/(x-x_center));
				winkel = (360/(2*PI))*winkel;
			}else if(x < x_center && y <= y_center){
			//2. quadrant up left
				winkel = java.lang.Math.atan((double)(y_center-y)/(x_center-x));
				winkel = 180 - (360/(2*PI))*winkel;
			}else if(x < x_center && y > y_center){
			//3. quadrant bottom left
				winkel = java.lang.Math.atan((double)(y-y_center)/(x_center-x));
				winkel = 180 + (360/(2*PI))*winkel;
			}else if(x >= x_center && y > y_center){
				winkel = java.lang.Math.atan((double)(y-y_center)/(x-x_center));
				winkel = 360 - (360/(2*PI))*winkel;
			}//if
			return winkel;
		}//calculateWinkel

		/**
		 * 
		 */
		public void paint(Graphics g){
			Graphics2D screen2D = (Graphics2D)g;
			int shorterside;
			if (getHeight() < getWidth()){
				shorterside = getHeight();
			}else{
				shorterside = getWidth();
			}//if
			int links =getWidth()/2 - (shorterside/2 - shorterside/20);
			x_center = links+((shorterside - shorterside/10)/2);
			y_center = shorterside/20 + ((shorterside - shorterside/10)/2);
			//paints the sectors yellow
			screen2D.setColor(Color.yellow);
			for (int i = 0; i < denominator; i++){
				if(filledsectors[i]){
					Arc2D.Float arc_numerator = new Arc2D.Float(
						links,
						shorterside/20,
						shorterside - 2*shorterside/20,
						shorterside - 2*shorterside/20,
						(float)(360.0/denominator)*i,
						(float)(360.0/denominator),
						Arc2D.PIE
					);//Arc
					screen2D.fill(arc_numerator);
				}//if
			}//for
			//draws the lines
			screen2D.setColor(Color.black);
			for ( int i = 0; i  <= denominator;  i ++){
				Arc2D.Float arc_i = new Arc2D.Float(
					links,
					shorterside/20,
					shorterside - 2*shorterside/20,
					shorterside - 2*shorterside/20,
					0,
					(float)(360.0/denominator)*i,
					Arc2D.PIE
				);//Arc
				screen2D.draw(arc_i);
			}//for
		}//paint

		/**
		 * 
		 */
		public void mouseClicked(MouseEvent e){
			owner.lb_info.setText("");
			double winkel = calculateWinkel(e.getX(), e.getY());
			
			if (e.getButton() == 1){//linke Maustaste
				for (int i = 0; i < denominator; i++){
					if(winkel>=(360.0/denominator)*i && winkel<=(360.0/denominator)*(i+1)){
						filledsectors[i] = true;
					}
				}//for
			}else if (e.getButton() == 3){
				for (int i = 0; i < denominator; i++){
					if(winkel>=(360.0/denominator)*i && winkel<=(360.0/denominator)*(i+1)){
						filledsectors[i] = false;
					}
				}//for
			}
			repaint();
		}//mouseClicked
		public void mousePressed(MouseEvent e){}
		public void mouseReleased(MouseEvent e){}
		public void mouseEntered(MouseEvent e){}
		public void mouseExited(MouseEvent e){}
	}//FractionAsClickCircle