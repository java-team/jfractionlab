/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.FractionAsRectangle;
import jfractionlab.displays.FractionLine;
import jfractionlab.exerciseGenerator.ExerciseGenerator;
import jfractionlab.jflDialogs.UsabilityDialog;
import jfractionlab.jflDialogs.WorkSheetDialog;

public class FractionToDecimal extends ExerciseDialog implements ActionListener, KeyListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	//GUI
	private JLabel lb_instruction = new JLabel("", JLabel.CENTER);
	private MyJTextField tf_numerator_1 = new MyJTextField(3);
	private MyJTextField tf_denominator_1 = new MyJTextField(3);
	private MyJTextField tf_numerator_2 = new MyJTextField(3);
	private JLabel lb_denominator_2 = new JLabel("", JLabel.CENTER);
	private MyJTextField tf_decimal = new MyJTextField(3);
	FractionAsRectangle pizza1 = new FractionAsRectangle();
	FractionAsRectangle pizza2 = new FractionAsRectangle();
	
	//Zahlen
	private FractionMaker fraction = new FractionMaker();
	private int numerator_1;
	private int denominator_1;
	private int denominator_2;
	/**
	 * 
	 * @param owner
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public FractionToDecimal(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx,ly,sx,sy);
		setTitle(lang.Messages.getString("fraction_to_decimal"));
		btn_continue = new JButton(lang.Messages.getString("continue"));
		btn_end =  new JButton(lang.Messages.getString("end"));
		this.owner = owner;
		//Menu
		rb_random.addActionListener(this);
		jmOptions.add(rb_random);
		rb_custom.addActionListener(this);
		jmOptions.add(rb_custom);
		jmb.add(jmOptions);
		jmiCreateWorkSheet.addActionListener(this);
		jmWorkSheet.add(jmiCreateWorkSheet);
		jmb.add(jmWorkSheet);
		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
//		
		double sizes[][] = {{
			//Spalten
			5,					//00
			TableLayout.FILL,	//01: 
			TableLayout.FILL,	//02: fraction1
			TableLayout.FILL,	//03:
			20,					//04: equal
			TableLayout.FILL,	//05:
			TableLayout.FILL,	//06: fraction2
			TableLayout.FILL,	//07
			20,					//08: equal
			TableLayout.FILL,	//09: decimal
			TableLayout.FILL,	//10: buttons
			5					//11
		}, {
			//Zeilen
			5,					//00
			TableLayout.FILL,	//01: instructionlabel
			0.1,				//02: numerator
			15,					//03: fractionbar
			0.1,	//04: denominator
			0.25,				//05: first pizza
			10,					//06
			0.25,				//07: second pizza
			TableLayout.FILL,	//08: infolabel
			5					//09
		}};
		Container content = getContentPane();
			content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
		
		lb_instruction.setFont(JFractionLab.infofont);
		content.add(lb_instruction, "1,1,9,1");
		
		//first fraction
		addTextField(tf_numerator_1);
		tf_numerator_1.addKeyListener(this);
		content.add(tf_numerator_1, "2,2,c,b");
		content.add(new FractionLine(), "2,3");
		addTextField(tf_denominator_1);
		tf_denominator_1.addKeyListener(this);
		content.add(tf_denominator_1, "2,4,c,t");
		
		//equal
		JLabel lb_equal1 = new JLabel("", JLabel.CENTER);
		lb_equal1.setFont(JFractionLab.infofont);
		lb_equal1.setText("=");
		content.add(lb_equal1, "4,3");
		
		//second fraction
		addTextField(tf_numerator_2);
		tf_numerator_2.addKeyListener(this);
		content.add(tf_numerator_2, "6,2,c,b");
		content.add(new FractionLine(), "6,3");
		content.add(lb_denominator_2, "6,4,c,t");
		
		//equal
		JLabel lb_equal2 = new JLabel("", JLabel.CENTER);
		lb_equal2.setFont(JFractionLab.infofont);
		lb_equal2.setText("=");
		content.add(lb_equal2, "8,3");
		
		//decimal
		addTextField(tf_decimal);
		tf_decimal.addKeyListener(this);
		content.add(tf_decimal, "9,2,9,4,c,c");
		
		
		//first pizza
		content.add(pizza1, "1,5,9,5");
		
		//second pizza
		content.add(pizza2, "1,7,9,7");
		
		//infolabel
		lb_info.setFont(JFractionLab.infofont);
		lb_info.setText(lang.Messages.getString("you_just_need_nb_and_enter"));
		content.add(lb_info, "1,8,9,8");
		
		//Buttons
		btn_continue.addKeyListener(this);
		btn_continue.addActionListener(this);
		content.add(btn_continue, "10,2,f,b");
		
		btn_end.addActionListener(this);
		content.add(btn_end, "10,4,f,t");
		
		points = owner.points_extendFraction;
		pdsp.setText(String.valueOf(points));
		content.add(pdsp, "10,5,10,7,c,c");
		
		makeProblem();
		String[] ar_howto = {"howto_nb_and_enter","howto_option_type_of_exercise"};
		new UsabilityDialog(ar_howto);
	}//Konstruktor
	
	/**
	 * 
	 *
	 */
	protected void makeProblem(){
		//System.out.println("makeProblem");
		fraction.mkDecimalFraction(true);
		numerator_1 = fraction.getNumerator_1();
		denominator_1 = fraction.getDenominator_1();
		denominator_2 = calculateDenominator2(denominator_1);
		tf_numerator_1.setText(String.valueOf(numerator_1));
		tf_denominator_1.setText(String.valueOf(denominator_1));
		lb_denominator_2.setText(String.valueOf(denominator_2));
		pizza1.drawPizzaAsRectangle(numerator_1, denominator_1, Color.yellow, false, false);
		pizza2.drawEmptyPizzaAsRectangle(denominator_2, false, false);
		tf_numerator_2.setEditable(true);
		tf_numerator_2.requestFocusInWindow();
	}
	
	private int calculateDenominator2(int dn1){
			System.out.println("dn 1 = "+dn1);
			if(dn1 == 0){
				System.out.println("mist dn1 ist 0!!!!");
				return 10;
			}else if(10%dn1==0){
				return 10;
			}else{
				return 100;
			}
	}
	/**
	 * 
	 *
	 */
	protected void nextProblem(){
		clearTextFields();
		lb_info.setText("");
		if (bl_randomProblem == true){
			makeProblem();
		}else{
			pizza1.noPizzaAsRectangle();
			pizza2.noPizzaAsRectangle();
			lb_denominator_2.setText("");
			tf_numerator_1.setEditable(true);
			tf_numerator_1.requestFocusInWindow();
		}
	}
	
	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
       	if (obj == btn_continue){
			nextProblem();
		}else if (obj == btn_end){
			close_it();
		}else if(obj == jmiHelp){
			new HelpStarter(
					lang.Messages.getLocale().toString(),
					"decimal" 
			);
		}else if(obj == rb_random){
			//System.out.println("rb_random");
			//the program is in "custom-mode" actually
			//it should switch to "random-mode"
			bl_randomProblem = true;
			nextProblem();
		}else if(obj == rb_custom){
			//System.out.println("rb_custom");
			//the program is in "random-mode" actually
			//it should switch to "custom-mode"
			bl_randomProblem = false;
			nextProblem();
		}else if(obj == jmiCreateWorkSheet){
			new WorkSheetDialog(
					ExerciseGenerator.FRACTIONS_TO_DECIMAL,
					lang.Messages.getString("fraction_to_decimal")
			);
		}
	}//actionPerformed
	
	public void keyPressed(KeyEvent event){
		Object obj = event.getSource();
		int key = event.getKeyCode();
		lb_info.setText("");
		if(obj == btn_continue & key == KeyEvent.VK_ENTER){
			nextProblem();
		}else if(obj == btn_end & key == KeyEvent.VK_ENTER){
			close_it();
		}else if(obj == tf_numerator_1 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_numerator_1) != null){
				int num = readInputInt(tf_numerator_1);
				numerator_1 = num;
				tf_numerator_1.setEditable(false);
				tf_denominator_1.setEditable(true);
				tf_denominator_1.requestFocusInWindow();
			}
		}else if(obj == tf_denominator_1 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_denominator_1) != null){
				int num = readInputInt(tf_denominator_1);
				boolean isOK= true;
				if(100%num != 0){
					lb_info.setText(lang.Messages.getString("denominator_divisible_by_hundret"));
					tf_denominator_1.setText("");
					isOK = false;
				}
				if(numerator_1 >= num){
					lb_info.setText(lang.Messages.getString("dn_must_be_bigger"));
					tf_denominator_1.setText("");
					tf_denominator_1.setEditable(false);
					tf_numerator_1.setEditable(true);
					tf_numerator_1.requestFocusInWindow();
					isOK = false;
				}
				if(isOK){
					tf_numerator_1.setEditable(false);
					denominator_1 = num;
					denominator_2 = calculateDenominator2(denominator_1);
					lb_denominator_2.setText(String.valueOf(denominator_2));
					pizza1.drawPizzaAsRectangle(numerator_1, denominator_1, Color.yellow, false, false);
					pizza2.drawEmptyPizzaAsRectangle(denominator_2, false, false);
					tf_numerator_2.setEditable(true);
					tf_numerator_2.requestFocusInWindow();
				}
			}
		}else if(obj == tf_numerator_2 & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_numerator_2) != null){
				int num = readInputInt(tf_numerator_2);
				if(num ==numerator_1*denominator_2/denominator_1){
					pizza2.drawPizzaAsRectangle(num, denominator_2, Color.yellow, false, false);
					tf_numerator_2.setEditable(false);
					tf_decimal.setEditable(true);
					tf_decimal.requestFocusInWindow();
				}else{
					if(num <= denominator_2){
						pizza2.drawPizzaAsRectangle(num, denominator_2, Color.red, false, false);
						lb_info.setText(numerator_1+" * "+(denominator_2/denominator_1)+" = ?");
					}else{
						pizza2.drawEmptyPizzaAsRectangle(denominator_2, false, false);
						lb_info.setText(lang.Messages.getString("numerator_is_too_big"));
					}
					tf_numerator_2.setText("");
				}
			}
		}else if(obj == tf_decimal & key == KeyEvent.VK_ENTER){
			if(readInputDouble(tf_decimal) != null){
				double num = readInputDouble(tf_decimal);
				if(num == ((double)numerator_1/denominator_1)){
					if(bl_randomProblem){
						points++;
					}
					pdsp.setText(String.valueOf(points));
					lb_info.setText(lang.Messages.getString("that_is_right"));
					owner.setPoints(points, "fractiontodecimal");
					btn_continue.requestFocusInWindow();
				}else{
					tf_decimal.setText("");
					lb_info.setText(lang.Messages.getString("fraction_to_decimal_example"));
				}
			}
		}
	}//keyPressed
	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){}
}//class
