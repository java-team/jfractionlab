/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.FractionAsCircle;
import jfractionlab.displays.FractionLine;
import jfractionlab.exerciseGenerator.ExerciseGenerator;
import jfractionlab.jflDialogs.UsabilityDialog;
import jfractionlab.jflDialogs.WorkSheetDialog;


/**
 * 
 * @author JochenGeorges
 *
 */
public class ImproperFraction extends ExerciseDialog implements ActionListener, KeyListener{
static final long serialVersionUID = JFractionLab.serialVersionUID;
	//GUI
	
	private MyJTextField tf_mixedNBNumber = new MyJTextField(2);
	private MyJTextField tf_mixedNBNumerator = new MyJTextField(2);
	private MyJTextField tf_mixedNBDenominator = new MyJTextField(2);
	
	private MyJTextField tf_improperFractionNumerator = new MyJTextField(2);
	private MyJTextField tf_improperFractionDenominator = new MyJTextField(2);

	private FractionAsCircle[] pizzaPlace = {
			new FractionAsCircle(),
			new FractionAsCircle(),
			new FractionAsCircle(),
			new FractionAsCircle(),
			new FractionAsCircle(),
			new FractionAsCircle(),
			new FractionAsCircle(),
			new FractionAsCircle(),
			new FractionAsCircle()
	};
	
	private JButton btn_tipp;
	private boolean isVisible = true;
	private boolean was_neverVisible = true;
	
	//Zahlen
	private FractionMaker fm;
	private int improperFractionNumerator;
	private int improperFractionDenominator;
	private int mixedNBNumber;
	private int mixedNbNumerator;
	/**
	 * 
	 * @param owner
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public ImproperFraction(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx, ly, sx, sy);
		setTitle(lang.Messages.getString("improper_frac_2_mixed_nb"));
		this.owner = owner;
		
		rb_random.addActionListener(this);
		jmOptions.add(rb_random);
		rb_custom.addActionListener(this);
		jmOptions.add(rb_custom);
		jmOptions.addSeparator();
		rb_visible.addActionListener(this);
		jmOptions.add(rb_visible);
		rb_invisible.addActionListener(this);
		jmOptions.add(rb_invisible);
		jmb.add(jmOptions);
		jmiCreateWorkSheet.addActionListener(this);
		jmWorkSheet.add(jmiCreateWorkSheet);
		jmb.add(jmWorkSheet);
		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
		
		
		double width_of_pizzaplace = 0.45;
		double sizes[][] = {{
			// Spalten
			5,//0
			width_of_pizzaplace/6,//1
			width_of_pizzaplace/6,//2
			width_of_pizzaplace/6,//3
			width_of_pizzaplace/6,//4
			width_of_pizzaplace/6,//5
			width_of_pizzaplace/6,//6
			12,//7
			width_of_pizzaplace/4,//8
			width_of_pizzaplace/4,//9
			width_of_pizzaplace/4,//10
			width_of_pizzaplace/4,//11
			TableLayout.FILL,//12
			5//13
		},{
			//Zeilen
			5,//0jflTextResource.properties
			TableLayout.FILL,//1
			20,//2
			TableLayout.FILL,//3
			0.4,//4
			0.4,//5
			TableLayout.FILL,//6
			5//7
		}};
		Container content = getContentPane();
		content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);
		//--links
			addTextField(tf_mixedNBNumber);
			tf_mixedNBNumber.addKeyListener(this);
			tf_mixedNBNumber.setEditable(false);
		content.add(tf_mixedNBNumber, "9,1,9,3,c,c");
			addTextField(tf_mixedNBNumerator);
			tf_mixedNBNumerator.addKeyListener(this);
			tf_mixedNBNumerator.setEditable(false);
		content.add(tf_mixedNBNumerator, "10,1,c,b");
			addTextField(tf_mixedNBDenominator);
			tf_mixedNBDenominator.setEditable(false);
		content.add(tf_mixedNBDenominator, "10,3,c,t");
		//--rechts
			addTextField(tf_improperFractionNumerator);
			tf_improperFractionNumerator.addKeyListener(this);
			tf_improperFractionNumerator.setEditable(false);
		content.add(tf_improperFractionNumerator, "3,1,4,1,c,b");
			addTextField(tf_improperFractionDenominator);
			tf_improperFractionDenominator.addKeyListener(this);
			tf_improperFractionDenominator.setEditable(false);
		content.add(tf_improperFractionDenominator, "3,3,4,3,c,t");

		//--Zeichen
		JLabel lb_gleich1 = new JLabel("=", JLabel.CENTER);
		content.add(lb_gleich1, "7,2");
		FractionLine bs_fraction1 = new FractionLine();
		content.add(bs_fraction1, "10,2");
		FractionLine bs_fraction2 = new FractionLine();
		content.add(bs_fraction2, "3,2,4,2");
		//--Pizzen
		content.add(pizzaPlace[0], "1,4,3,4");
		content.add(pizzaPlace[1], "1,5,3,5");
		content.add(pizzaPlace[2], "4,4,6,4");
		content.add(pizzaPlace[3], "4,5,6,5");
		content.add(pizzaPlace[4], "8,4,9,4");
		content.add(pizzaPlace[5], "8,5,9,5");
		content.add(pizzaPlace[6], "10,4,11,4");
		content.add(pizzaPlace[7], "10,5,11,5");
		//--Controls
			btn_continue = new JButton(lang.Messages.getString("continue"));
			btn_continue.addActionListener(this);
			btn_continue.addKeyListener(this);
			btn_continue.setFocusTraversalKeysEnabled(false);
		content.add(btn_continue, "12,1,c,c");
		
			btn_tipp = new JButton(lang.Messages.getString("Tipp"));
			btn_tipp.addActionListener(this);
			btn_tipp.setEnabled(false);
		content.add(btn_tipp, "12,4,f,c");
		
			btn_end =  new JButton(lang.Messages.getString("end"));
			btn_end.addActionListener(this);
		content.add(btn_end, "12,3,c,c");
		content.add(pdsp, "12,5");
			lb_info.setFont(JFractionLab.infofont);
			lb_info.setText(lang.Messages.getString("you_just_need_nb_and_enter"));
		content.add(lb_info, "1,6,11,6");

		points = owner.points_unechteBrueche;
		pdsp.setText(String.valueOf(points));
		makeProblem();
		String[] ar_howto = {
			"howto_nb_and_enter","howto_option_type_of_exercise","howto_option_invisible"
		};
		new UsabilityDialog(ar_howto);//new UsabilityDialog
	}//Konstruktor
	
	/**
	 * 
	 *
	 */
	protected void makeProblem(){
		//System.out.println("=== makeProblem ===");
		fm = new FractionMaker(); //der Parameter setzt die exclusive Obergrenze der Zahlen des Bruches
		fm.mkImproperFraction();
		improperFractionNumerator = fm.getNumerator_1();
		improperFractionDenominator = fm.getDenominator_1();
			//TESTZAHLEN
			//improperFractionNumerator = 29; improperFractionDenominator = 17;
		if(!isVisible){
			tf_improperFractionNumerator.setText(String.valueOf(improperFractionNumerator));
			tf_improperFractionDenominator.setText(String.valueOf(improperFractionDenominator));
		}
		mixedNBNumber = improperFractionNumerator/improperFractionDenominator;
		//System.out.println("mixedNBNumber = "+ mixedNBNumber);
		mixedNbNumerator = improperFractionNumerator - (improperFractionDenominator * mixedNBNumber);
		//System.out.println("mixedNbNumerator = "+ mixedNbNumerator);
		was_neverVisible = true;
		if(!isVisible){
			tf_mixedNBNumber.setEditable(true);
			tf_mixedNBNumber.requestFocusInWindow();
		}else{
			drawPizzaBlech();
			tf_improperFractionNumerator.setEditable(true);
			tf_improperFractionNumerator.requestFocusInWindow();
			lb_info.setText(lang.Messages.getString("which_improper_fraction_do_you_see"));
		}
	}//makeProblem()
	
	/**
	 * 
	 *
	 */
	private void drawPizzaBlech(){
		int nb = 0;
		for(int i = 0; i < mixedNBNumber; i++){
			pizzaPlace[i].drawPizzaAsCircle(improperFractionDenominator, improperFractionDenominator, Color.yellow);
			nb = i;
		}//for
		if(mixedNbNumerator != 0){
			pizzaPlace[nb+1].drawPizzaAsCircle(
					mixedNbNumerator,
					improperFractionDenominator,
					Color.yellow);
		}else{
			pizzaPlace[nb+1].noPizzaAsCircle();
		}//if	
		was_neverVisible = false;
	}//drawPizzaBlech
	
	/**
	 * 
	 *
	 */
	private void clearPizzaBlech(){
		for(int i = 0; i < 8; i++){
			pizzaPlace[i].noPizzaAsCircle();
		}//for
	}//clearPizzaBlech	
	
	/**
	 * 
	 *
	 */
	protected void nextProblem(){
		for(int i = 0; i < 8; i++){
			pizzaPlace[i].noPizzaAsCircle();
		}//for
		clearTextFields();
		if (bl_randomProblem == true){
			makeProblem();
		}else{
			tf_improperFractionNumerator.setEditable(true);
			tf_improperFractionNumerator.requestFocusInWindow();
		}//if
	}

	/**
	 * 
	 *
	 *
	private void close_it(){
		setVisible(false);
		dispose();
	}//close_it()
*/
	/**
	 *
	 *
	 */
	public void itemStateChanged(ItemEvent e){
		//System.out.println("\n ------ \n");
		//System.out.println("itemStateChanged");
		//Object obj = e.getSource();
	}//itemStateChanged

	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn_continue){
			nextProblem();
			if (isVisible){
				btn_tipp.setEnabled(false);
			}else{
				btn_tipp.setEnabled(true);
			}
		}else if (obj == btn_end){
			close_it();
		}else if (obj == btn_tipp){
			//System.out.println("btn_tipp");
			drawPizzaBlech();
			btn_tipp.setEnabled(false);
			tf_mixedNBNumber.requestFocusInWindow();
		}else if (obj == rb_visible){
			//System.out.println("rb_visble");
			//the program is in "Invisible-mode" actually
			//but it should switch to "Visible-mode"
			drawPizzaBlech();
			btn_tipp.setEnabled(false);
			isVisible = true;
		}else if (obj == rb_invisible){
			//System.out.println("rb_invisble");
			//the program is in "Visible-mode" actually
			//it should switch to "Invisible-mode"
			clearPizzaBlech();
			btn_tipp.setEnabled(true);
			isVisible = false;
			makeProblem();
		}else if (obj == rb_random){
			//System.out.println("rb_random");
			//the program is in "custom-mode" actually
			//it should switch to "random-mode"
			bl_randomProblem = true;
			nextProblem();
		}else if (obj == rb_custom){
			//System.out.println("rb_custom");
			//the program is in "random-mode" actually
			//it should switch to "custom-mode"
			bl_randomProblem = false;
			nextProblem();
		}else if(obj == jmiCreateWorkSheet){
			new WorkSheetDialog(
					ExerciseGenerator.IMPROPER_FRACTIONS,
					lang.Messages.getString("improper_fracs"),
					lang.Messages.getString("fractions")
			);
		}else if(obj == jmiHelp){
			new HelpStarter(
					lang.Messages.getLocale().toString(),
					"improper-fractions"
			);
		}
	}//actionPerformed
	
	/**
	 * 
	 */
	public void keyPressed(KeyEvent event){
		Object obj = event.getSource();
		int key = event.getKeyCode();
		lb_info.setText("");
		if(obj == btn_continue & key == KeyEvent.VK_ENTER){
			nextProblem();
		}else if(obj == tf_improperFractionNumerator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_improperFractionNumerator)!= null){
				int num = readInputInt(tf_improperFractionNumerator);
				if(bl_randomProblem){
					if(num == improperFractionNumerator){
						tf_improperFractionNumerator.setEditable(false);
						tf_improperFractionDenominator.setEditable(true);
						tf_improperFractionDenominator.requestFocusInWindow();
					}else{
						tf_improperFractionNumerator.setText("");
						tf_improperFractionNumerator.requestFocusInWindow();
					}
				}else{
					improperFractionNumerator = Integer.parseInt(tf_improperFractionNumerator.getText());
					tf_improperFractionNumerator.setEditable(false);
					tf_improperFractionDenominator.setEditable(true);
					tf_improperFractionDenominator.requestFocusInWindow();
				}
			}else{
				tf_improperFractionNumerator.setText("");
				tf_improperFractionNumerator.setEditable(true);
			}
		}else if(obj == tf_improperFractionDenominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_improperFractionDenominator) != null){
				int num = Integer.parseInt(tf_improperFractionDenominator.getText()); 
				if(bl_randomProblem){
					if(num == improperFractionDenominator){
						tf_improperFractionDenominator.setEditable(false);
						tf_mixedNBNumber.setEditable(true);
						tf_mixedNBNumber.requestFocusInWindow();
					}else{
						tf_improperFractionDenominator.setText("");
						tf_improperFractionDenominator.requestFocusInWindow();
					}
				}else{
					if(num >= improperFractionNumerator){
						tf_improperFractionDenominator.setText("");
						lb_info.setText(lang.Messages.getString("denom_is_too_big"));
					}else{
						if(improperFractionNumerator / num < 8){
							improperFractionDenominator = num;
							mixedNBNumber = improperFractionNumerator / improperFractionDenominator;
							mixedNbNumerator= improperFractionNumerator - mixedNBNumber * improperFractionDenominator;
							if(isVisible){drawPizzaBlech();}
							tf_improperFractionDenominator.setEditable(false);
							tf_mixedNBNumber.setEditable(true);
							tf_mixedNBNumber.requestFocusInWindow();
							tf_mixedNBDenominator.setText(String.valueOf(improperFractionDenominator));
						}else{
							lb_info.setText(lang.Messages.getString("maximise_num"));
							tf_improperFractionDenominator.setText("");
						}	
					}
				}
			}//readInputNumber
		}else if(obj == tf_mixedNBNumber & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_mixedNBNumber) != null){
				int nb = Integer.parseInt(tf_mixedNBNumber.getText());
				if(nb != mixedNBNumber){
					lb_info.setText(lang.Messages.getString("how_often_does_denom_fits_in_num"));
					tf_mixedNBNumber.setText("");
				}else{
					if(mixedNbNumerator == 0){
						System.out.println("ergebnis ist glatte ganzzahl");
						tf_mixedNBNumber.setEditable(false);
						if(bl_randomProblem){
							points++;
						}
						if(bl_randomProblem && !isVisible && was_neverVisible){
							//extra point
							points++;
						}
						lb_info.setText(lang.Messages.getString("that_is_right"));
						pdsp.setText(String.valueOf(points));
						owner.setPoints(points, "unechteBrueche");
						drawPizzaBlech();
						btn_continue.requestFocusInWindow();
					}else{
						tf_mixedNBNumber.setEditable(false);
						tf_mixedNBNumerator.setEditable(true);
						tf_mixedNBDenominator.setText(String.valueOf(improperFractionDenominator));
						tf_mixedNBNumerator.requestFocusInWindow();
					}
				}
			}//if(readInput....)
		}else if(obj == tf_mixedNBNumerator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_mixedNBNumerator) != null){
				int nb = Integer.parseInt(tf_mixedNBNumerator.getText());
				if(nb != mixedNbNumerator){
					lb_info.setText(lang.Messages.getString("how_many_are_left"));
					tf_mixedNBNumerator.setText("");
				}else{
					tf_mixedNBNumerator.setEditable(false);
					if(bl_randomProblem){
						points++;
					}
					if(bl_randomProblem && !isVisible && was_neverVisible){
						//extra point
						points++;
					}
					lb_info.setText(lang.Messages.getString("that_is_right"));
					pdsp.setText(String.valueOf(points));
					owner.setPoints(points, "unechteBrueche");
					drawPizzaBlech();
					btn_continue.requestFocusInWindow();
				}
			}//readInputNumber
		}
	}//keyPressed
	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){}
	
}//class
