/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/


package jfractionlab.exerciseDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import jfractionlab.FractionMaker;
import jfractionlab.HelpStarter;
import jfractionlab.JFractionLab;
import jfractionlab.MyJTextField;
import jfractionlab.displays.FractionAsCircle;
import jfractionlab.displays.FractionLine;
import jfractionlab.jflDialogs.UsabilityDialog;


public class NameFraction extends ExerciseDialog implements ActionListener, KeyListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	//GUI
	private MyJTextField tf_numerator = new MyJTextField(2);
	private MyJTextField tf_denominator = new MyJTextField(2);
	private FractionAsCircle pp_pizza = new FractionAsCircle();
	//Zahlen
	private FractionMaker fraction;
	private int numerator;
	private int denominator;
	
	/**
	 * 
	 * @param owner
	 * @param lx
	 * @param ly
	 * @param sx
	 * @param sy
	 * @throws HeadlessException
	 */
	public NameFraction(JFractionLab owner, int lx, int ly, int sx, int sy) throws HeadlessException {
		super(lx, ly, sx, sy);
		setTitle(lang.Messages.getString("name_the_fraction"));
		btn_continue = new JButton(lang.Messages.getString("continue"));
		btn_end =  new JButton(lang.Messages.getString("end"));
		this.owner = owner;
		
		jmiHelp.addActionListener(this);
		jmb.add(jmHelp);
		setJMenuBar(jmb);
		
		double sizes[][] = {{	
			// Spalten
			TableLayout.FILL,
			0.045,
			TableLayout.FILL,
			0.20,
			12
		},{	
			//Zeilen
			12,
			30,
			12,
			30,
			TableLayout.FILL,
			TableLayout.FILL,
			12,
			TableLayout.FILL,
			TableLayout.FILL,
			30
		}};
		Container content = getContentPane();
		content.setLayout(new TableLayout(sizes));
		content.setBackground(Color.white);

			addTextField(tf_numerator);
			tf_numerator.addKeyListener(this);
		content.add(tf_numerator, "1,1,c,b");
			FractionLine bs = new FractionLine();
		content.add(bs, "1,2");
			addTextField(tf_denominator);
			tf_denominator.addKeyListener(this);
			tf_denominator.setEditable(false);
		content.add(tf_denominator, "1,3,c,t");
		content.add(pp_pizza, "0,4,2,8");
			btn_continue.addActionListener(this);
			btn_continue.addKeyListener(this);
			btn_continue.setFocusTraversalKeysEnabled(false);
		content.add(btn_continue, "3,5,f,b");
			btn_end.addActionListener(this);
			btn_end.addKeyListener(this);
		content.add(btn_end, "3,7,f,t");
		content.add(pdsp, "3,8,3,9");
		lb_info.setFont(JFractionLab.infofont);
		lb_info.setText(lang.Messages.getString("you_just_need_nb_and_enter"));
		content.add(lb_info, "0,9,2,9");

		points = owner.points_nameFraction;
		pdsp.setText(String.valueOf(points));
		makeProblem();
		String[] ar_howto = {"howto_nb_and_enter"};
		new UsabilityDialog(ar_howto);//new UsabilityDialog
	}//Konstruktor
	
	protected void nextProblem(){}
	
	/**
	 * 
	 *
	 */
	public void makeProblem(){
		clearTextFields();
		//pp_pizza.noPizzaAsCircle();
		//System.out.println("makeProblem");
		fraction = new FractionMaker();
		fraction.mkOneFraction(10);
		numerator = fraction.getNumerator_1();
		denominator = fraction.getDenominator_1();
		pp_pizza.drawPizzaAsCircle(numerator, denominator, Color.yellow);
		tf_numerator.setEditable(true);
		tf_numerator.requestFocusInWindow();
	}

	/**
	 * 
	 */
	public void actionPerformed (ActionEvent e) {
		Object obj = e.getSource();
		if (obj == btn_continue){
			JOptionPane.showMessageDialog(
				null,
				lang.Messages.getString("no_btn_use_enter")
			);
			tf_numerator.requestFocusInWindow();
		}else if (obj == btn_end){
			close_it();
		}else if(obj == jmiHelp){
			new HelpStarter(
					lang.Messages.getLocale().toString(),
					"fractions-explanation"
			);
		}
	}//actionPerformed

	/**
	 * 
	 */
	public void keyPressed(KeyEvent event){
		Object obj = event.getSource();
		int key = event.getKeyCode();
		lb_info.setText("");
		if(obj == btn_continue & key == KeyEvent.VK_ENTER){
			makeProblem();
		}else if(obj == btn_end & key == KeyEvent.VK_ENTER){
			close_it();
		}else if(obj == tf_numerator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_numerator) != null){
				if(numerator != readInputInt(tf_numerator)){
					tf_numerator.setText("");
					lb_info.setText(lang.Messages.getString("wrong_numerator"));
				}else{
					tf_numerator.setEditable(false);
					tf_denominator.setEditable(true);
					tf_denominator.requestFocusInWindow();
				}
			}
		}else if(obj == tf_denominator & key == KeyEvent.VK_ENTER){
			if(readInputInt(tf_denominator) != null){
				if(denominator != readInputInt(tf_denominator)){
					lb_info.setText(lang.Messages.getString("wrong_denominator"));
					tf_denominator.setText("");
				}else{
					points++;
					pdsp.setText(String.valueOf(points));
					owner.setPoints(points, "nameFraction");
					lb_info.setText(lang.Messages.getString("that_is_right")+" "+lang.Messages.getString("press_enter"));
					btn_continue.requestFocusInWindow();
				}
			}
		}
	}//keyPressed
	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){}
}//class