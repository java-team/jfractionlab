package jfractionlab.exerciseGenerator;

/**
 * The ExerciseGenerators make one (1) ExerciseObject.
 * @author jochen
 *
 */
public class ExerciseGenerator {

	public static final int EXTEND_FRACTIONS = 1;
	public static final int REDUCE_FRACTIONS = 2;
	public static final int IMPROPER_FRACTIONS = 3;
	public static final int MIXED_NUMBERS = 4;
	public static final int FRACTIONS_TO_DECIMAL = 5;
	public static final int ADD_FRACTIONS = 6;
	public static final int SUBTRACT_FRACTIONS = 7;
	public static final int MULTIPLY_FRACTIONS = 8;
	public static final int DIVIDE_FRACTIONS = 9;
	
//	public ExerciseGenerator(){}
	
	public Exercises getOneTypeExercises(
			int type,
			int max1,
			int max2,
			int nb_of_exercises
	){
		boolean isOK = true;
		
		Exercises ex = new Exercises("");
		Exercises provi = new Exercises("");

		GeneratorObject generator = new GeneratorOfNullExercises();
		
		if(type == EXTEND_FRACTIONS){
			ex.setTitle(lang.Messages.getString("extend_fractions"));
			generator = new GeneratorOfExtendFractionsExercises();
		}else if(type == REDUCE_FRACTIONS){
			ex.setTitle(lang.Messages.getString("reduce_fractions"));
			generator = new GeneratorOfReduceFractionExercises();
		}else if(type == IMPROPER_FRACTIONS){
			ex.setTitle(lang.Messages.getString("improper_fracs"));
			generator = new GeneratorOfImproperFractionExercises();
		}else if(type == MIXED_NUMBERS){
			ex.setTitle(lang.Messages.getString("mixed_numbers"));
			generator = new GeneratorOfMixedNumbersExercises();
		}else if(type == FRACTIONS_TO_DECIMAL){
			ex.setTitle(lang.Messages.getString("fraction_to_decimal"));
			generator = new GeneratorOfFractionToDecimalExercise();
		}else if(type == ADD_FRACTIONS){
			ex.setTitle(lang.Messages.getString("add_fractions"));
			generator = new GeneratorOfAdditionExercises();
		}else if(type == SUBTRACT_FRACTIONS){
			ex.setTitle(lang.Messages.getString("minus_fractions"));
			generator = new GeneratorOfSubtractionExercises();
		}else if(type == MULTIPLY_FRACTIONS){
			ex.setTitle(lang.Messages.getString("multiply_fractions"));
			generator = new GeneratorOfMultiplicationExercises();
		}else if(type == DIVIDE_FRACTIONS){
			ex.setTitle(lang.Messages.getString("div_fr"));
			generator = new GeneratorOfDivisionExercises();
		}
		else{
			//Fehler
			isOK = false;
		}
		
		if(isOK){
			for(int i = 0; i<nb_of_exercises;i++){
				provi = generator.getOneExercise(max1, max2);

				String aufgabe = provi.getAlExercises().get(0);
				ex.getAlExercises().add(aufgabe);
				
				String kl = provi.getAlSolutions().get(0);
				ex.getAlSolutions().add(kl);
				
				String ll = provi.getAlCalculations().get(0);
				ex.getAlCalculations().add(ll);
			}
		}
		return ex;
	}
	
	public Exercises getDiversTypeExercises(
			int[] type,
			int[] max1,
			int[] max2
	){
		//die Arrays müssen gleich lang sein!!
		if(
				type.length == max1.length &&
				type.length == max2.length &&
				max1.length == max2.length
		){
			
		}else{
			//fehler laenge der arrays ist unterschiedlich
		}
		
		return new Exercises("");
	}

}
