package jfractionlab.jflDialogs;

import info.clearthought.layout.TableLayout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.FocusTraversalPolicy;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import jfractionlab.ConfManager;
import jfractionlab.CopyDir;
import jfractionlab.JFractionLab;
import jfractionlab.officeMachine.OfficeStarter;
import lang.Messages;

public class WorkSheetDialog extends JDialog implements ActionListener{
	static final long serialVersionUID = JFractionLab.serialVersionUID;
	
	private Container content;
	private JLabel lbTitle = new JLabel("", JLabel.CENTER);
	private String title="";

	private int type;
	private String titleOptions="";
	private ButtonGroup btngrp_Options = new ButtonGroup();
	private JRadioButton rb_10 = new JRadioButton("0 - 10");
	private JRadioButton rb_100 = new JRadioButton("0 - 100");

	private String titleOptionsDouble="";
	private ButtonGroup btngrp_OptionsDouble = new ButtonGroup();
	private JRadioButton rb_double_10 = new JRadioButton("0 - 10");
	private JRadioButton rb_double_100 = new JRadioButton("0 - 100");

	private String templatePath="";
	private JTextField jtfTemplatePath = new JTextField();
	private JButton btnFC = new JButton(" ... ");
	
	private JButton btnOK = new JButton();
	private JButton btnCancel = new JButton();
	private double sizes_main[][];
	private MyOwnFocusTraversalPolicy newPolicy = new MyOwnFocusTraversalPolicy();

	private int nb_of_optionFields;
	private File selectedFile;
	
	//ONLY_FILE_CHOOSER
	public WorkSheetDialog(int type, String title){
		super();
		this.nb_of_optionFields = 0;
		myConstructor(type,title, "", "");
	}
	
	//ONE_OPTIONS_FIELD
	public WorkSheetDialog(int type, String title, String titleOptions){
		super();
		this.nb_of_optionFields = 1;
		myConstructor(type,title,titleOptions,"");
	}
	
	//TWO_OPTIONS_FILED
	public WorkSheetDialog(
			int type,
			String title,
			String titleOptions,
			String titleOptionsDouble
	){
		super();
		this.nb_of_optionFields = 2;
		myConstructor(type,title,titleOptions,titleOptionsDouble);
	}
	
	public void myConstructor(
			int type,
			String title,
			String titleOptions,
			String titleOptionsDouble
	){
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		this.type = type;
		this.title = lang.Messages.getString("worksheets")+" : "+title;
		if(!titleOptions.equals("")){
			this.titleOptions = lang.Messages.getString("range_of_numbers")+" : "+titleOptions;
		}
		if(!titleOptionsDouble.equals("")){
			this.titleOptionsDouble = lang.Messages.getString("range_of_numbers")+" : "+titleOptionsDouble;
		}
		createGUI();
	}
	
	private void createGUI(){
		setTitle(lang.Messages.getString("generate_worksheets"));
		if(nb_of_optionFields == 0){
			sizes_main = new double[][] {{
				5,
				TableLayout.FILL,
				TableLayout.FILL,
				5
			},{
				5,
				0.20,
				TableLayout.FILL,
				0.20,
				5
			}};  //Spalten / Zeilen
		}else if(nb_of_optionFields == 1){
			sizes_main = new double[][] {{
				5,
				TableLayout.FILL,
				TableLayout.FILL,
				5
			},{
				5,
				0.20,
				TableLayout.FILL,
				TableLayout.FILL,
				0.20,
				5
			}};  //Spalten / Zeilen
		}else if(nb_of_optionFields == 2){
			sizes_main = new double[][] {{
				5,
				TableLayout.FILL,
				TableLayout.FILL,
				5
			},{
				5,
				0.15,
				TableLayout.FILL,
				TableLayout.FILL,
				TableLayout.FILL,
				0.15,
				5
			}}; //Spalten / Zeilen
		}
		getRootPane().setDefaultButton(btnOK);
		content = getContentPane();
		content.setLayout(new TableLayout(sizes_main));
		lbTitle.setText(title);
		content.add(lbTitle,"1,1,2,1");
		
		if(nb_of_optionFields == 0){
			content.add(createFileChooser(),"1,2,2,2");
		}else if(nb_of_optionFields == 1){
			content.add(createOptions(false), "1,2,2,2");
			content.add(createFileChooser(),"1,3,2,3");
		}else if(nb_of_optionFields == 2){
			content.add(createOptions(false), "1,2,2,2");
			content.add(createOptions(true), "1,3,2,3");
			content.add(createFileChooser(),"1,4,2,4");
		}
		
		btnCancel.setText(lang.Messages.getString("cancel"));
		btnCancel.addActionListener(this);
		btnOK.setText(lang.Messages.getString("OK"));
		btnOK.addActionListener(this);

		if(nb_of_optionFields == 0){
			content.add(btnCancel, "1,3,c,c");
			content.add(btnOK, "2,3,c,c");
		}else if(nb_of_optionFields == 1){
			content.add(btnCancel, "1,4,c,c");
			content.add(btnOK, "2,4,c,c");
		}else if(nb_of_optionFields == 2){
			content.add(btnCancel, "1,5,c,c");
			content.add(btnOK, "2,5,c,c");
		}
		
		
		content.setFocusTraversalPolicy(newPolicy);
		setLocation(50, 150);
		
		if(nb_of_optionFields == 0){
			setSize(500,210);
		}else if(nb_of_optionFields == 1){
			setSize(500,280);
		}else if(nb_of_optionFields == 2){
			setSize(500,350);
		}

		setResizable(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
	}

	private JPanel createOptions(boolean isDouble){
		double sizes_options[][];
		sizes_options = new double[][] {{
			5,
			TableLayout.FILL,
			TableLayout.FILL,
			5
		},{
			5,
			TableLayout.FILL,
			TableLayout.FILL,
			TableLayout.FILL,
			5
		}}; //Spalten / Zeilen
		JPanel pnOptions = new JPanel();
		pnOptions.setLayout(new TableLayout(sizes_options));
		pnOptions.setBorder(BorderFactory.createEtchedBorder());
		if(isDouble==false){
			JLabel lbOptionsTitle = new JLabel(titleOptions, JLabel.CENTER);
			pnOptions.add(lbOptionsTitle, "1,1");
			pnOptions.add(rb_10, "1,2");
			pnOptions.add(rb_100, "1,3");
			btngrp_Options.add(rb_10);
			btngrp_Options.add(rb_100);
			rb_10.setSelected(true);
		}else{
			JLabel lbOptionsTitle = new JLabel(titleOptionsDouble, JLabel.CENTER);
			pnOptions.add(lbOptionsTitle, "1,1");
			pnOptions.add(rb_double_10, "1,2");
			pnOptions.add(rb_double_100, "1,3");
			btngrp_OptionsDouble.add(rb_double_10);
			btngrp_OptionsDouble.add(rb_double_100);
			rb_double_10.setSelected(true);
		}
		return pnOptions;
	}
	
	private JPanel createFileChooser(){
		double sizes_fc[][];
		sizes_fc = new double[][] {{
			5,
			0.85,
			TableLayout.FILL,
			5
		},{
			5,
			30,
			TableLayout.FILL,
			5
		}}; //Spalten / Zeilen
		JPanel pnFC = new JPanel();
		pnFC.setLayout(new TableLayout(sizes_fc));
		pnFC.setBorder(BorderFactory.createEtchedBorder());
		
		pnFC.add(new JLabel(lang.Messages.getString("get_odt_template")),"1,1,2,1");
		pnFC.add(jtfTemplatePath, "1,2");
		btnFC.addActionListener(this);
		pnFC.add(btnFC,"2,2");
		return pnFC;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if(obj == btnFC){
			fcAction();
		}else if (obj == btnCancel){
				close();
		}else if(obj == btnOK){
			if(templatePath.equals("")){
				JOptionPane.showMessageDialog(null, lang.Messages.getString("get_odt_template"));
			}else{
				okAction();
				close();
			}
		}
	}

	public void okAction(){
		int max1 = 10;
		int max2 = 10;
		
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		
		if(nb_of_optionFields > 0){
			if(rb_10.isSelected()){
				max1 = 10;
			}else{
				max1 = 100;
			}
			System.out.println("max1 = "+max1);
		}
		if(nb_of_optionFields == 2){
			if(rb_double_10.isSelected()){
				max2 = 10;
			}else{
				max2= 100;
			}
			System.out.println("max2 = "+max2);
		}
		new OfficeStarter(
				this,
				selectedFile,
				type,
				max1,
				max2
		);
		close();
	}
	
	private void fcAction(){
		JFileChooser fc = new JFileChooser();
		FileFilter myfilter = new FileNameExtensionFilter("odt", "ODT");
		fc.setFileFilter(myfilter);
		fc.setLocale(lang.Messages.getLocale());
		
		String str_templatedirectory = new ConfManager().getConfdir()+System.getProperty("file.separator")+"templates";
		File templatedirectory = new File(str_templatedirectory);

		//FIXME neue oder geaenderte vorlagen kopieren
		if(!(templatedirectory.exists())){
			File srcdir = new File("");
			boolean bl = false;
			if(new File("/usr/share/jfractionlab/templates/").exists()){
				srcdir = new File("/usr/share/jfractionlab/templates/");
				bl = true;
			}else if(new File("C:\\Programme\\JFractionLab\\templates").exists()){
				srcdir = new File("C:\\Programme\\JFractionLab\\templates");
				bl = true;
			}else{
				JOptionPane.showMessageDialog(null, Messages.getString("find_the_templates")+"\n"+str_templatedirectory);
			}
//			System.out.println("srcdir = "+srcdir.toString());
			if(bl){
				try {
					CopyDir.copyDir(srcdir , new File(str_templatedirectory));
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Can not copy the files from\n"+
							srcdir.toString()+" to "+str_templatedirectory+"\n"+
							"This should not happen. Please send a bug report to gnugeo@gnugeo.de\n" +
							"Thanks !!"
							);
					templatedirectory = srcdir;
				}
			}
		}
		
		fc.setCurrentDirectory(templatedirectory);
		
		int returnVal = fc.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			templatePath = fc.getSelectedFile().toString();
			this.selectedFile = fc.getSelectedFile();
		}else{
			templatePath = "";
		}
		jtfTemplatePath.setText(templatePath);
	}
	
	private void close(){
		setVisible(false);
		dispose();
	}
	
	public class MyOwnFocusTraversalPolicy extends FocusTraversalPolicy {
		@Override
		public Component getComponentAfter(Container aContainer,Component aComponent) {
			//soll: btn_yes, btn_no,
			if(aComponent.equals(jtfTemplatePath)) {
				return btnFC;
			}else if(aComponent.equals(btnFC)) {
				return btnCancel;
			}else if(aComponent.equals(btnCancel)){
				return btnOK;
			}else if(aComponent.equals(btnOK)) {
				return btnCancel;
			}else{
				return btnOK;
			}
		}

		@Override
		public Component getComponentBefore(Container aContainer,Component aComponent) {
			//soll: btn_yes, btn_no
			if(aComponent.equals(btnOK)) {
				return btnCancel;
			}else if(aComponent.equals(btnCancel)){
				return btnFC;
			}else if(aComponent.equals(btnFC)){
				return jtfTemplatePath;
			}else{
				return jtfTemplatePath;
			}
		}

		@Override
		public Component getDefaultComponent(Container aContainer) {
			return btnOK;
		}

		@Override
		public Component getFirstComponent(Container aContainer) {
			return jtfTemplatePath;
		}

		@Override
		public Component getLastComponent(Container aContainer) {return null;}
	}//MyOwnFocusTraversalPolicy
}