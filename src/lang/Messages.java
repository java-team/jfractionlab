/**
 *	JFractionLab
 *	Copyright (C) 2005 jochen georges, gnugeo _ at _ gnugeo _ dot _ de
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

package lang;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author jochen
 *
 */
public class Messages {
	public static Locale mylocale = Locale.getDefault();
	private static String bundlebody = "lang.txtsrc";
	private static String BUNDLE_NAME = bundlebody+"_de";

	private static ResourceBundle RESOURCE_BUNDLE =
		ResourceBundle.getBundle(BUNDLE_NAME, mylocale); 

	public Messages() {}
	
	public static void setLocale(Locale loc){
		mylocale = loc;
		if(mylocale == Locale.GERMAN){
			BUNDLE_NAME = bundlebody+"_de";
		}else if(mylocale == Locale.FRANCE){
			BUNDLE_NAME = bundlebody+"_fr";
		}if(mylocale == new Locale("es")){
			BUNDLE_NAME = bundlebody+"_es";
		}if(mylocale == Locale.ENGLISH){
			BUNDLE_NAME = bundlebody+"_en";
		}if(mylocale == Locale.ITALIAN){
			BUNDLE_NAME = bundlebody+"_it";
		}if(mylocale == new Locale("pt")){
			BUNDLE_NAME = bundlebody+"_pt";
		}else{
			BUNDLE_NAME = bundlebody;
		}
		
		
		RESOURCE_BUNDLE =
			ResourceBundle.getBundle(BUNDLE_NAME, mylocale);
		//System.out.println("setLocale ende");
	}//setLocale
	
	public static Locale getLocale(){
		return mylocale;
	}
	
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}//class Messages